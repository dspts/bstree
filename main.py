class CategoryNode:
    """
    This is a helper class that represents a category node.
    """
    def __init__(self, value):
        self.children = list()
        self.value = value

    def add_child(self, value):
        """
        Add child to category tree node.
        :param value: tree value
        :return:
        """
        self.children.append(value)


class CategoryTree:
    """
    This is a class to represent a tree of categories.
    Nodes are given in the form (parent, child) and parent of root is None
    Included is a helper method to represent the tree.
    """

    def __init__(self):
        self.categories = list()
        self.parents = list()
        self.children = list()
        self.root_is_taken = False

    def add(self, node):
        """
        Add category node to tree. Each node is given in the form (parent, child).
        Use a field children to each node, in order to be able to print instantly the node's children.
        :param node:
        :return:
        """
        node_parent = node[0]
        node_value = node[1]
        if (node_parent not in self.parents) and (node_parent is None):
            # check if root has been added to avoid unconnected tree
            if self.root_is_taken:
                raise ValueError
            treenode = CategoryNode(node_parent)
            treenode.add_child(node_value)
            self.parents.append(node_parent)
            self.children.append(node_value)
            self.categories.append(treenode)
            self.root_is_taken = True
        elif node_parent not in self.children:  # every parent should be already added to the tree since it's not root
            raise ValueError
        else:
            # find the category node in categories list and use it else create new node
            for category in self.categories:
                if category.value == node_parent:
                    treenode = category
                    treenode.add_child(node_value)
                    self.children.append(node_value)
                    break
            else:
                treenode = CategoryNode(node_parent)
                treenode.add_child(node_value)
                self.parents.append(node_parent)
                self.children.append(node_value)
                self.categories.append(treenode)

    def represent(self):
        """
        Helper method to print the category tree.
        :return:
        """
        for category in self.categories:
            print("Value: " + str(category.value))
            print("Children: " + str(category.children))
            print("")


if __name__ == '__main__':
    ctree = CategoryTree()
    ctree.add((None, 'A'))  # root
    # ctree.add((None, 'B')) # correctly raises exception
    ctree.add(('A', 'B'))
    ctree.add(('A', 'C'))
    ctree.add(('B', 'D'))
    ctree.add(('B', 'E'))
    # ctree.add(('Z', 'H')) # correctly raises exception
    ctree.represent()
